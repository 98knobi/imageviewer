﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.IO.Compression;
using System.Collections.Generic;

using Newtonsoft.Json;

using Emgu.CV;
using Emgu.CV.Structure;

namespace Library {
    public class RenderPipeline {
        public const string BaseDirectoryPath = "./temp";        
        public const string ProjectDataFileName = "pipeline.json";
        public const string ProjectFileExtension = "simg";

        public string ImageResourcesFolderName { get => "imageResources"; }

        private string imageFolderPath;

        public List<IRenderStep> RenderSteps { get; set; }

        private string workdirectoryPath;

        public RenderPipeline() {
            RenderSteps = new List<IRenderStep>();

            if (!Directory.Exists(BaseDirectoryPath))
                Directory.CreateDirectory(BaseDirectoryPath);

            workdirectoryPath = BaseDirectoryPath + "/" + DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            if (!Directory.Exists(workdirectoryPath))
                Directory.CreateDirectory(workdirectoryPath);

            imageFolderPath = $"{workdirectoryPath}/{ImageResourcesFolderName}";

            if (!Directory.Exists(imageFolderPath))
                Directory.CreateDirectory(imageFolderPath);
        }

        ~RenderPipeline() {
            Directory.Delete(workdirectoryPath, true);
        }

        public IRenderStep AddImageLayer(string imageFilePath) {
            string fileExtension = imageFilePath.Substring(imageFilePath.LastIndexOf('.'));

            int imageCount = this.RenderSteps
                .Where(rs => rs.GetType() == typeof(ImageLayer)).Count();

            string tempFilename = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds + fileExtension;

            File.Copy(imageFilePath, imageFolderPath + "/" + tempFilename);

            ImageLayer newImageLayer = new ImageLayer {
                ImageFolder = imageFolderPath,
                Filename = tempFilename,
                ScaleX = 1,
                ScaleY = 1
            };

            this.RenderSteps.Add(newImageLayer);

            return newImageLayer;
        }

        public void SaveProjectFile(string targetPath) {
            RenderPipeline tempRenderPipeline = this;

            string jsonData = JsonConvert.SerializeObject(tempRenderPipeline, Formatting.None, new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.Auto
            });

            tempRenderPipeline = null;

            if (File.Exists($"{workdirectoryPath}/{ProjectDataFileName}"))
                File.Delete($"{workdirectoryPath}/{ProjectDataFileName}");

            StreamWriter projectFile = File.CreateText($"{workdirectoryPath}/{ProjectDataFileName}");
            projectFile.Write(jsonData);
            projectFile.Close();

            ZipFile.CreateFromDirectory(workdirectoryPath, targetPath);
        }

        public void OpenProjectFile(string filePath) {
            if (File.Exists($"{workdirectoryPath}/{ProjectDataFileName}"))
                File.Delete($"{workdirectoryPath}/{ProjectDataFileName}");

            Directory.Delete(imageFolderPath, true);

            ZipFile.ExtractToDirectory(filePath, workdirectoryPath);
            this.RenderSteps.Clear();
            StreamReader sr = File.OpenText(workdirectoryPath + "/" + ProjectDataFileName);
            this.RenderSteps = JsonConvert.DeserializeObject<RenderPipeline>(sr.ReadToEnd(), new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.Auto
            }).RenderSteps;

            foreach (ImageLayer imgl in this.RenderSteps.Where(rs => rs.GetType() == typeof(ImageLayer)))
                imgl.ImageFolder = this.imageFolderPath;
        }

        public void ExportAsImage(string filePath) {
            foreach(IRenderStep step in this.RenderSteps) {
                step.OutputImage = null;
            }

            Image<Bgra, byte> outputImage = this.Render();

            outputImage.Bitmap.Save(filePath);
        }

        public Image<Bgra, byte> Render() {
            Image<Bgra, byte> outputImage = null;
            IRenderStep prevStepHelper = null;

            foreach(IRenderStep step in RenderSteps) {
                if (prevStepHelper != null)
                    step.PrevStep = prevStepHelper;

                outputImage = step.OutputImage ?? step.Render();
                prevStepHelper = step;
            }

            return outputImage;
        }
    }

    public interface IRenderStep {
        Image<Bgra, byte> Render();

        [JsonIgnore]
        IRenderStep PrevStep { get; set; }

        [JsonIgnore]
        Image<Bgra, byte> OutputImage { get; set; }
    }
    
    public class ImageLayer : IRenderStep {

        private string _filename { get; set; }
        private IRenderStep _prevStep { get; set; }
        private PointF _position { get; set; }
        private float _rotation { get; set; }
        private float _scaleX { get; set; }
        private float _scaleY { get; set; }

        [JsonIgnore]
        public string ImageFolder { get; set; }

        public string Filename {
            get => this._filename;
            set {
                this._filename = value;
                this.OutputImage = null;
            }
        }

        public IRenderStep PrevStep {
            get => this._prevStep;
            set {
                this._prevStep = value;
                this.OutputImage = null;
            }
        }

        public PointF Position {
            get => this._position;
            set {
                this._position = value;
                this.OutputImage = null;
            }
        }

        public float Rotation {
            get => this._rotation;
            set {
                this._rotation = value;
                this.OutputImage = null;
            }
        }

        public float ScaleX {
            get => this._scaleX;
            set {
                this._scaleX = value;
                this.OutputImage = null;
            }
        }

        public float ScaleY {
            get => this._scaleY;
            set {
                this._scaleY = value;
                this.OutputImage = null;
            }
        }

        public Image<Bgra, byte> OutputImage { get; set; }

        public Image<Bgra, byte> Render() {
            Image<Bgra, byte> workImage = new Image<Bgra, byte>(ImageFolder + "/" + Filename);

            Image<Bgra, byte> outputImage;

            Image<Bgra, byte> prevImage;

            if (this._prevStep == null)
                prevImage = new Image<Bgra, byte>(workImage.Size);
            else
                prevImage = _prevStep.OutputImage.Clone();

            Bitmap bmp = prevImage.Bitmap;

            float moveX = workImage.Width / 2f + _position.X;
            float moveY = workImage.Height / 2f + _position.Y;

            Graphics graph = Graphics.FromImage(bmp);
            graph.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
            graph.TranslateTransform(moveX, moveY);
            graph.RotateTransform(this._rotation);
            graph.ScaleTransform(this.ScaleX, this.ScaleY);
            graph.TranslateTransform(-moveX, -moveY);
            graph.DrawImage(workImage.Bitmap, _position);

            outputImage = prevImage;
            OutputImage = outputImage;

            return outputImage;
        }
    }

    public class Filter : IRenderStep {
        protected IRenderStep _prevStep { get; set; }
        public IRenderStep PrevStep {
            get => this.PrevStep;
            set {
                this._prevStep = value;
                this.OutputImage = null;
            }
        }

        public Image<Bgra, byte> OutputImage { get; set; }

        public virtual Image<Bgra, byte> Render() {
            throw new NotImplementedException();
        }
    }

    public class FilterBlur : Filter {
        private int _blurRadius { get; set; }

        public int BlurRadius {
            get => this._blurRadius;
            set {
                this._blurRadius = value;
                this.OutputImage = null;
            }
        }

        public override Image<Bgra, byte> Render() {
            OutputImage = this._prevStep.OutputImage.SmoothGaussian(_blurRadius);
            return OutputImage;
        }
    }

    public class FilterMedian : Filter {
        private int _size { get; set; }

        public int Size {
            get => this._size;
            set {
                this._size = value;
                this.OutputImage = null;
            }
        }

        public override Image<Bgra, byte> Render() {
            OutputImage = this._prevStep.OutputImage.SmoothMedian(_size);
            return OutputImage;
        }
    }

    public class OperationRotate : Filter {
        private float _angle { get; set; }

        public float Angle {
            get => this._angle;
            set {
                this._angle = value;
                this.OutputImage = null;
            }
        }

        public override Image<Bgra, byte> Render() {            
            OutputImage = this._prevStep.OutputImage.Rotate(_angle, new Bgra(0,0,0,0));
            return OutputImage;
        }
    }

    public class OperationFlip : Filter {
        public bool Horizontal { get; set; }
        public bool Vertical { get; set; }

        public override Image<Bgra, byte> Render() {
            Image<Bgra, byte> helper = this._prevStep.OutputImage.Clone();
            if (Horizontal)
                helper = helper.Flip(Emgu.CV.CvEnum.FlipType.Horizontal);
            if (Vertical)
                helper = helper.Flip(Emgu.CV.CvEnum.FlipType.Vertical);

            OutputImage = helper;
            return OutputImage;
        }
    }    
}
