﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Collections.Generic;

using MahApps.Metro.Controls;
using System.Windows;

namespace ImageViewer {
    public partial class MainWindow : MetroWindow {
        private IEnumerable<string> allowedExtensions = new List<string> { ".jpg", ".png", ".gif", ".bmp" };

        public MainWindow() {
            ToolTipService.ShowDurationProperty.OverrideMetadata(
                typeof(DependencyObject), new FrameworkPropertyMetadata(Int32.MaxValue));

            InitializeComponent();

            string[] cmdLine = Environment.GetCommandLineArgs();

            Page startupPage = new Pages.Viewer.Main();

            if (cmdLine.Length > 1) {
                string filePath = cmdLine[1];

                if (File.Exists(filePath)) {
                    if (Path.GetExtension(filePath) == ".simg") {
                        startupPage = new Pages.Editor.Main(filePath);
                    }
                    else if (allowedExtensions.Contains(Path.GetExtension(filePath))) {
                        startupPage = new Pages.Viewer.Main(filePath);
                    }
                }
            }

            frmMain.Content = startupPage;
        }
    }
}
