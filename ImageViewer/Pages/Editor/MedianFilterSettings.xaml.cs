﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

using Library;

namespace ImageViewer.Pages.Editor {
    public partial class MedianFilterSettings : Page {
        private FilterMedian _filterMedian { get; set; }
        private Action _changeCallback { get; set; }
        private RenderPipeline _pipeline { get; set; }

        private int? _size { get; set; }

        public int Size {
            get => _size ?? _filterMedian.Size;
            set {
                if (value % 2 == 0)
                    value++;
                if (value <= 0)
                    value = 1;
                _size = value;
                _filterMedian.Size = Size;
                _changeCallback.Invoke();
            }
        }

        public MedianFilterSettings(Action changeCallback, FilterMedian filterMedian, RenderPipeline pipeline) {
            InitializeComponent();

            DataContext = this;

            _filterMedian = filterMedian;
            _changeCallback = changeCallback;
            _pipeline = pipeline;
        }

        private void CloseSettings(object sender, RoutedEventArgs e) {
            Frame pageFrame = null;
            DependencyObject currentParent = VisualTreeHelper.GetParent(this);
            while (currentParent != null && pageFrame == null) {
                pageFrame = currentParent as Frame;
                currentParent = VisualTreeHelper.GetParent(currentParent);
            }

            if (pageFrame != null)
                pageFrame.Content = "";
        }

        private void DeleteRenderStep(object sender, RoutedEventArgs e) {
            if (MessageBox.Show("Do you really want to delete this Render Step?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
                _pipeline.RenderSteps.Remove(_filterMedian);
                _changeCallback.Invoke();
                CloseSettings(null, null);
            }
        }
    }
}
