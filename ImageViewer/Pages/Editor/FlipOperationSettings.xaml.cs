﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

using Library;

namespace ImageViewer.Pages.Editor {
    public partial class FlipOperationSettings : Page {
        private OperationFlip _operationFlip { get; set; }
        private Action _changeCallback { get; set; }        
        private RenderPipeline _pipeline { get; set; }

        private bool? _vertical { get; set; }
        private bool? _horizontal { get; set; }

        public bool Vertical {
            get => _vertical ?? _operationFlip.Vertical;
            set {
                _vertical = value;
                _operationFlip.Vertical = Vertical;
                _changeCallback.Invoke();
            }
        }

        public bool Horizontal {
            get => _horizontal ?? _operationFlip.Horizontal;
            set {
                _horizontal = value;
                _operationFlip.Horizontal = Horizontal;
                _changeCallback.Invoke();
            }
        }

        public FlipOperationSettings(Action changeCallback, OperationFlip operationFlip, RenderPipeline pipeline) {
            InitializeComponent();

            DataContext = this;            

            _operationFlip = operationFlip;
            _changeCallback = changeCallback;
            _pipeline = pipeline;
        }

        private void CloseSettings(object sender, RoutedEventArgs e) {
            Frame pageFrame = null;
            DependencyObject currentParent = VisualTreeHelper.GetParent(this);
            while (currentParent != null && pageFrame == null) {
                pageFrame = currentParent as Frame;
                currentParent = VisualTreeHelper.GetParent(currentParent);
            }

            if (pageFrame != null)
                pageFrame.Content = "";
        }

        private void DeleteRenderStep(object sender, RoutedEventArgs e) {
            if (MessageBox.Show("Do you really want to delete this Render Step?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
                _pipeline.RenderSteps.Remove(_operationFlip);
                _changeCallback.Invoke();
                CloseSettings(null, null);
            }
        }
    }
}
