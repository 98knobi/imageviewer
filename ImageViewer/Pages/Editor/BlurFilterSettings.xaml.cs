﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

using Library;

namespace ImageViewer.Pages.Editor {
    public partial class BlurFilterSettings : Page {
        private FilterBlur _filterBlur { get; set; }
        private Action _changeCallback { get; set; }
        private RenderPipeline _pipeline { get; set; }

        private int? _blurRadius { get; set; }

        public int BlurRadius {
            get => _blurRadius ?? _filterBlur.BlurRadius;
            set {
                if (value % 2 == 0)
                    value++;
                if (value <= 0)
                    value = 1;
                _blurRadius = value;
                _filterBlur.BlurRadius = BlurRadius;
                _changeCallback.Invoke();
            }
        }

        public BlurFilterSettings(Action changeCallback, FilterBlur filterBlur, RenderPipeline pipeline) {
            InitializeComponent();

            DataContext = this;

            _filterBlur = filterBlur;
            _changeCallback = changeCallback;
            _pipeline = pipeline;
        }

        private void CloseSettings(object sender, RoutedEventArgs e) {
            Frame pageFrame = null;
            DependencyObject currentParent = VisualTreeHelper.GetParent(this);
            while (currentParent != null && pageFrame == null) {
                pageFrame = currentParent as Frame;
                currentParent = VisualTreeHelper.GetParent(currentParent);
            }

            if (pageFrame != null)
                pageFrame.Content = "";
        }

        private void DeleteRenderStep(object sender, RoutedEventArgs e) {
            if (MessageBox.Show("Do you really want to delete this Render Step?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes) { 
                _pipeline.RenderSteps.Remove(_filterBlur);
                _changeCallback.Invoke();
                CloseSettings(null, null);
            }
        }
    }
}
