﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

using Library;

namespace ImageViewer.Pages.Editor {
    public partial class RotateOperationSettings : Page {
        private OperationRotate _operationRotate { get; set; }
        private Action _changeCallback { get; set; }
        private RenderPipeline _pipeline { get; set; }

        private float? _angle { get; set; }

        public float Angle {
            get => _angle ?? _operationRotate.Angle;
            set {
                _angle = value;
                _operationRotate.Angle = Angle;
                _changeCallback.Invoke();
            }
        }

        public RotateOperationSettings(Action changeCallback, OperationRotate operationRotate, RenderPipeline pipeline) {
            InitializeComponent();

            DataContext = this;

            _operationRotate = operationRotate;
            _changeCallback = changeCallback;
            _pipeline = pipeline;
        }

        private void CloseSettings(object sender, RoutedEventArgs e) {
            Frame pageFrame = null;
            DependencyObject currentParent = VisualTreeHelper.GetParent(this);
            while (currentParent != null && pageFrame == null) {
                pageFrame = currentParent as Frame;
                currentParent = VisualTreeHelper.GetParent(currentParent);
            }

            if (pageFrame != null)
                pageFrame.Content = "";
        }

        private void DeleteRenderStep(object sender, RoutedEventArgs e) {
            if (MessageBox.Show("Do you really want to delete this Render Step?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
                _pipeline.RenderSteps.Remove(_operationRotate);
                _changeCallback.Invoke();
                CloseSettings(null, null);
            }
        }
    }
}
