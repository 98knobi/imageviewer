﻿using System;
using System.Linq;
using System.Drawing;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

using Library;

namespace ImageViewer.Pages.Editor {
    public partial class ImageSettings : Page {
        private ImageLayer _imgLayer { get; set; }
        private Action _changeCallback { get; set; }
        private RenderPipeline _pipeline { get; set; }

        private float? _posX { get; set; }
        private float? _posY { get; set; }        
        private float? _rotation { get; set; }
        private float? _scaleX { get; set; }
        private float? _scaleY { get; set; }

        public float PosX {
            get => _posX ?? _imgLayer.Position.X;
            set {
                _posX = value;
                _imgLayer.Position = new PointF(PosX, PosY);
                _changeCallback.Invoke();
            }
        }

        public float PosY {
            get => _posY ?? _imgLayer.Position.Y;
            set {
                _posY = value;
                _imgLayer.Position = new PointF(PosX, PosY);
                _changeCallback.Invoke();
            }
        }

        public float Rotation {
            get => _rotation ?? _imgLayer.Rotation;
            set {
                _rotation = value;
                _imgLayer.Rotation = Rotation;
                _changeCallback.Invoke();
            }
        }

        public float ScaleX {
            get => _scaleX ?? _imgLayer.ScaleX;
            set {
                if (value <= 0)
                    value = 0.1f;

                _scaleX = value;
                _imgLayer.ScaleX = ScaleX;
                _changeCallback.Invoke();
            }
        }

        public float ScaleY {
            get => _scaleY ?? _imgLayer.ScaleY;
            set {
                if (value <= 0)
                    value = 0.01f;
                _scaleY = value;
                _imgLayer.ScaleY = ScaleY;
                _changeCallback.Invoke();
            }
        }

        public ImageSettings(Action changeCallback, ImageLayer imgLayer, RenderPipeline pipeline) {
            InitializeComponent();

            DataContext = this;

            _imgLayer = imgLayer;
            _changeCallback = changeCallback;
            _pipeline = pipeline;

            if (pipeline.RenderSteps.First() == imgLayer)
                buDeleteRenderStep.IsEnabled = false;
        }

        private void CloseSettings(object sender, RoutedEventArgs e) {
            Frame pageFrame = null;
            DependencyObject currentParent = VisualTreeHelper.GetParent(this);
            while (currentParent != null && pageFrame == null) {
                pageFrame = currentParent as Frame;
                currentParent = VisualTreeHelper.GetParent(currentParent);
            }

            if (pageFrame != null)
                pageFrame.Content = "";
        }

        private void DeleteRenderStep(object sender, RoutedEventArgs e) {
            if (MessageBox.Show("Do you really want to delete this Render Step?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
                _pipeline.RenderSteps.Remove(_imgLayer);
                _changeCallback.Invoke();
                CloseSettings(null, null);
            }
        }
    }
}
