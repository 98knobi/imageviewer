﻿using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Collections.Generic;

using Emgu.CV.Structure;

using Library;
using System.Windows.Input;
using System;
using System.Collections.ObjectModel;

namespace ImageViewer.Pages.Editor {
    public partial class Main : Page {
        private RenderPipeline editorPipeline;
        
        public ObservableCollection<System.Windows.Controls.Button> RenderStepButtons { get; private set; }

        private SettingsCommand _ShowSettingsCommand;

        public ICommand ShowSettingsCommand {
            get {
                if (_ShowSettingsCommand == null)
                    _ShowSettingsCommand = new SettingsCommand(showSettings);
                return _ShowSettingsCommand;
            }
        }        

        public Main(string filePath) {            
            InitializeComponent();
            DataContext = this;

            editorPipeline = new RenderPipeline();

            if (Path.GetExtension(filePath) == ".simg") {
                editorPipeline.OpenProjectFile(filePath);
                renderEditorPipeline();
            }
            else { 
                AddImageToEditorPipeline(filePath);
            }

            MainWindow mw = (MainWindow)System.Windows.Application.Current.MainWindow;
            mw.Title = "Image Editor";
        }

        private void updateEditorPipelinePreview() {
            if (RenderStepButtons == null)
                RenderStepButtons = new ObservableCollection<System.Windows.Controls.Button>();

            RenderStepButtons.Clear();
            foreach (IRenderStep step in editorPipeline.RenderSteps) {
                System.Windows.Controls.Button buRenderStep = new System.Windows.Controls.Button();
                Image imageControl = new Image();

                if (step.OutputImage != null)
                    imageControl.Source = ImageUtility.BitmapToImageSource(step.OutputImage.Bitmap);

                buRenderStep.Content = imageControl;
                buRenderStep.CommandParameter = step;
                buRenderStep.Command = ShowSettingsCommand;

                RenderStepButtons.Add(buRenderStep);
            }            
        }

        private void renderEditorPipeline() {
            imgOutput.Source = ImageUtility.BitmapToImageSource(editorPipeline.Render().Bitmap);
            updateEditorPipelinePreview();
        }        

        private void showSettings(IRenderStep renderStep) {
            Type renderStepType = renderStep.GetType();
            Page toolSettingsPage = null;

            if (renderStepType == typeof(ImageLayer)) 
                toolSettingsPage = new ImageSettings(renderEditorPipeline, renderStep as ImageLayer, editorPipeline);
            else if (renderStepType == typeof(FilterBlur)) 
                toolSettingsPage = new BlurFilterSettings(renderEditorPipeline, renderStep as FilterBlur, editorPipeline);
            else if (renderStepType == typeof(FilterMedian))
                toolSettingsPage = new MedianFilterSettings(renderEditorPipeline, renderStep as FilterMedian, editorPipeline);
            else if (renderStepType == typeof(OperationRotate))
                toolSettingsPage = new RotateOperationSettings(renderEditorPipeline, renderStep as OperationRotate, editorPipeline);
            else if (renderStepType == typeof(OperationFlip))
                toolSettingsPage = new FlipOperationSettings(renderEditorPipeline, renderStep as OperationFlip, editorPipeline);

            if (toolSettingsPage != null)
                frmToolSettings.Content = toolSettingsPage;
        }        

        private void OpenImageAsNewLayer(object sender, RoutedEventArgs e) {
            FileDialog fd = new OpenFileDialog();

            if (fd.ShowDialog() == DialogResult.OK) {
                AddImageToEditorPipeline(fd.FileName);
            }
        }

        private void AddFilterBlur(object sender, RoutedEventArgs e) {
            IRenderStep newStep = new FilterBlur { BlurRadius = 1 };
            editorPipeline.RenderSteps.Add(newStep);
            showSettings(newStep);
            renderEditorPipeline();
        }

        private void AddFilterMedian(object sender, RoutedEventArgs e) {
            IRenderStep newStep = new FilterMedian { Size = 1 };
            editorPipeline.RenderSteps.Add(newStep);
            showSettings(newStep);
            renderEditorPipeline();
        }        

        private void AddOperationRotate(object sender, RoutedEventArgs e) {
            IRenderStep newStep = new OperationRotate();
            editorPipeline.RenderSteps.Add(newStep);
            showSettings(newStep);
            renderEditorPipeline();
        }

        private void AddOperationFlip(object sender, RoutedEventArgs e) {
            IRenderStep newStep = new OperationFlip();
            editorPipeline.RenderSteps.Add(newStep);
            showSettings(newStep);
            renderEditorPipeline();
        }

        private void AddImageToEditorPipeline(string imageFilePath) {
            showSettings(editorPipeline.AddImageLayer(imageFilePath));

            renderEditorPipeline();
        }

        private void SaveEditorPipeline(object sender, RoutedEventArgs e) {
            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "Project File | *.simg";
            sd.DefaultExt = ".simg";

            if (sd.ShowDialog() == DialogResult.OK) {
                if (File.Exists(sd.FileName))
                    File.Delete(sd.FileName);

                editorPipeline.SaveProjectFile(sd.FileName);
            }
        }

        private void OpenProjectFile(object sender, RoutedEventArgs e) {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Project File | *.simg";
            od.DefaultExt = ".simg";

            if (od.ShowDialog() == DialogResult.OK) {
                editorPipeline.OpenProjectFile(od.FileName);
                renderEditorPipeline();
            }
        }

        private void ExportAs(object sender, RoutedEventArgs e) {
            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "(*.png)|*.png|(*.jpg)|*.jpg";

            if (sd.ShowDialog() == DialogResult.OK) {
                if (File.Exists(sd.FileName))
                    File.Delete(sd.FileName);

                editorPipeline.ExportAsImage(sd.FileName);
            }
        }

        private void ExitEditor(object sender, RoutedEventArgs e) {
            if (System.Windows.MessageBox.Show("Do you really want to Exit the Editor?\nUnsaved work may be lost!", "Confirm Exit", MessageBoxButton.YesNo) == MessageBoxResult.Yes) { 
                Viewer.Main viewerPage = new Viewer.Main();
                var mainWindow = (MainWindow)System.Windows.Application.Current.MainWindow;
                mainWindow.frmMain.Content = viewerPage;
            }
        }

        private void ExitApplication(object sender, RoutedEventArgs e) {
            if (System.Windows.MessageBox.Show("Do you really want to Exit the App?\nUnsaved work may be lost!", "Confirm Exit", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                System.Windows.Application.Current.Shutdown();
        }
    }

    public class SettingsCommand : ICommand {
        public event EventHandler CanExecuteChanged;

        private Action<IRenderStep> _execute;

        public SettingsCommand(Action<IRenderStep> execute) {
            _execute = execute;
        }

        public bool CanExecute(object parameter) {
            return true;
        }

        public void Execute(object parameter) {
            _execute.Invoke(parameter as IRenderStep);
        }
    }    
}
