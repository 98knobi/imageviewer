﻿using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Drawing.Imaging;
using System.Windows.Controls;
using System.Collections.Generic;

using Emgu.CV;
using Emgu.CV.Structure;

namespace ImageViewer.Pages.Info {
    public partial class Main : Page {
        public Main() {
            InitializeComponent();
        }

        public List<ImageMetadata> Metadata { get; set; }

        private static Dictionary<int, string> ExifTags = new Dictionary<int, string> {
            { 0x010d, "Document Name" },
            { 0x010e, "Image Description" },
            { 0x9003, "Taken on" },
            { 0x010f, "Camera Manufacturer" },
            { 0x0110, "Camera Model" }
        };

        public void loadImageInfo(Bitmap bmp) {
            Metadata = new List<ImageMetadata>();
            PropertyItem[] props = bmp.PropertyItems;         

            ASCIIEncoding encoding = new ASCIIEncoding();

            foreach (PropertyItem prop in props) {
                if (ExifTags.ContainsKey(prop.Id)) { 
                    
                    Metadata.Add(new ImageMetadata {
                        Name = ExifTags[prop.Id],
                        Value = encoding.GetString(prop.Value).TrimEnd('\0')
                    });
                }
            }
            
            imageInfo.ItemsSource = Metadata;
            if (Metadata.Count <= 0)
                imageInfo.Visibility = Visibility.Hidden;
            else
                imageInfo.Visibility = Visibility.Visible;

            DrawHistogramToCanvas(canvasHisto, bmp);
        }

        public void DrawHistogramToCanvas(Canvas targetCanvas, Bitmap bmp) {
            Image<Bgr, byte> image = new Image<Bgr, byte>(bmp);

            DenseHistogram histo = new DenseHistogram(255, new RangeF(0, 255));

            float[] histoBlue;
            float[] histoGreen;
            float[] histoRed;

            float[] histoGray;

            Image<Gray, byte> imgBlue = image[0];
            Image<Gray, byte> imgGreen = image[1];
            Image<Gray, byte> imgRed = image[2];

            Image<Gray, byte> imgGray = image.Convert<Gray, byte>();

            histo.Clear();

            histo.Calculate(new Image<Gray, byte>[] { imgBlue }, true, null);
            histoBlue = histo.GetBinValues();

            histo.Clear();

            histo.Calculate(new Image<Gray, byte>[] { imgGreen }, true, null);
            histoGreen = histo.GetBinValues();

            histo.Clear();

            histo.Calculate(new Image<Gray, byte>[] { imgRed }, true, null);
            histoRed = histo.GetBinValues();

            histo.Clear();

            histo.Calculate(new Image<Gray, byte>[] { imgGray }, true, null);
            histoGray = histo.GetBinValues();

            float maxVal = 0;
            for (int i = 0; i < 255; i++) {
                if (maxVal < histoBlue[i]) maxVal = histoBlue[i];
                if (maxVal < histoGreen[i]) maxVal = histoGreen[i];
                if (maxVal < histoRed[i]) maxVal = histoRed[i];

                if (maxVal < histoGray[i]) maxVal = histoGray[i];
            }

            float relativeFactor = maxVal / (float)targetCanvas.Height;

            IEnumerable<histoChannel> histoChannels = new List<histoChannel> {
                new histoChannel { Histo = histoBlue, Brush = System.Windows.Media.Brushes.Blue },
                new histoChannel { Histo = histoGreen, Brush = System.Windows.Media.Brushes.Green },
                new histoChannel { Histo = histoRed, Brush = System.Windows.Media.Brushes.Red },
                new histoChannel { Histo = histoGray, Brush = System.Windows.Media.Brushes.Gray }
            };

            foreach (histoChannel channel in histoChannels) {
                for (int i = 0; i < targetCanvas.Width; i++) {
                    Line line = new Line();
                    line.Stroke = channel.Brush;

                    int histoIndex = (int)(i / targetCanvas.Width * 254);

                    line.X1 = i;
                    line.Y1 = targetCanvas.Height - channel.Histo[histoIndex] / relativeFactor;

                    line.X2 = i + 1;
                    line.Y2 = targetCanvas.Height - channel.Histo[histoIndex + 1] / relativeFactor;

                    targetCanvas.Children.Add(line);
                }
            }
        }

        private class histoChannel {
            public float[] Histo { get; set; }
            public System.Windows.Media.Brush Brush { get; set; }
        }

        private void closeImageInfo(object sender, RoutedEventArgs e) {
            Frame pageFrame = null;
            DependencyObject currentParent = VisualTreeHelper.GetParent(this);
            while (currentParent != null && pageFrame == null) {
                pageFrame = currentParent as Frame;
                currentParent = VisualTreeHelper.GetParent(currentParent);
            }

            if (pageFrame != null)
                pageFrame.Content = "";
            
        }
    }

    public class ImageMetadata {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
