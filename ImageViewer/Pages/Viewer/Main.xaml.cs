﻿using System.IO;
using System.Linq;
using System.Windows;
using System.Drawing;
using System.Windows.Input;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

using Library;
using System.Windows.Forms;

namespace ImageViewer.Pages.Viewer {    
    public partial class Main : Page {
        private Bitmap _activeImage { get; set; }

        private Bitmap activeImage {
            get => _activeImage;
            set {
                menuImage.IsEnabled = true;
                frmImageInfo.Content = null;
                _activeImage = value;
            }
        }

        private string activeImagePath;
        private string activeFolder;
        private List<string> imagesInFolder;

        private IEnumerable<string> allowedExtensions = new List<string> { ".jpg", ".png", ".gif", ".bmp" };        

        public Main(string filePath = "") {
            InitializeComponent();

            DataContext = this;
            
            if (filePath != "") {
                activeImage = new Bitmap(filePath);
                activeImagePath = filePath;

                // readout the folder the selected image is in
                activeFolder = Directory.GetParent(filePath).FullName;

                // search for all image files in the same directory
                imagesInFolder = Directory.GetFiles(activeFolder, "*.*", SearchOption.TopDirectoryOnly)
                    .Where(s => allowedExtensions.Contains(Path.GetExtension(s))).ToList();

                // render out the images
                renderThumbnails();
                renderActiveImage();
            }

            MainWindow mw = (MainWindow)System.Windows.Application.Current.MainWindow;
            mw.Title = "Image Viewer";
        }

        // use the generic open file dialog to load a image
        private void openImage(object sender, RoutedEventArgs e) {
            FileDialog fd = new OpenFileDialog();
            fd.Title = "Open Image";
            fd.Filter = "Image File|";

            foreach (string ext in allowedExtensions) {
                fd.Filter += "*" + ext + ";";
            }

            fd.Filter = fd.Filter.TrimEnd(';');

            fd.Filter += @"|Project File|*.simg";            

            if (fd.ShowDialog() == DialogResult.OK) {       
                if (Path.GetExtension(fd.FileName) == ".simg") {
                    OpenProjectInEditor(fd.FileName);
                }
                else { 
                    activeImage = new Bitmap(fd.FileName);
                    activeImagePath = fd.FileName;

                    // readout the folder the selected image is in
                    activeFolder = Directory.GetParent(fd.FileName).FullName;

                    // search for all image files in the same directory
                    imagesInFolder = Directory.GetFiles(activeFolder, "*.*", SearchOption.TopDirectoryOnly)
                        .Where(s => allowedExtensions.Contains(Path.GetExtension(s))).ToList();

                    // render out the images
                    renderThumbnails();
                    renderActiveImage();
                }
            }
        }

        // checks the active for other images and renders thumbnails for those
        private void renderThumbnails() {
            int index = imagesInFolder.FindIndex(ip => ip == activeImagePath);

            int iNext = index + 1 >= imagesInFolder.Count ? 0 : index + 1;
            int iPrev = index - 1 < 0 ? imagesInFolder.Count - 1 : index - 1;

            if (imagesInFolder.Count >= 2)
                imgThumbnailNext.Source = ImageUtility.BitmapToImageSource(new Bitmap(imagesInFolder[iNext]));

            if (imagesInFolder.Count >= 3)
                imgThumbnailPrev.Source = ImageUtility.BitmapToImageSource(new Bitmap(imagesInFolder[iPrev]));

            imgThumbnailActive.Source = ImageUtility.BitmapToImageSource(activeImage);
        }

        // converts the active image to an bitmap image and apply it to the main image control 
        private void renderActiveImage() {
            BitmapImage bmpi = ImageUtility.BitmapToImageSource(activeImage);

            imgMain.Source = bmpi;
        }

        private void loadPrevImage(object sender, RoutedEventArgs e) {
            if (activeImagePath != null) { 
                int index = imagesInFolder.FindIndex(ip => ip == activeImagePath) - 1;
                index = index < 0 ? imagesInFolder.Count - 1 : index;

                activeImagePath = imagesInFolder[index];

                activeImage = new Bitmap(activeImagePath);
                renderThumbnails();
                renderActiveImage();
            }
        }

        private void loadNextImage(object sender, RoutedEventArgs e) {
            if (activeImagePath != null) { 
                int index = imagesInFolder.FindIndex(ip => ip == activeImagePath) + 1;
                index = index > imagesInFolder.Count - 1 ? 0 : index;

                activeImagePath = imagesInFolder[index];

                activeImage = new Bitmap(activeImagePath);
                renderThumbnails();
                renderActiveImage();
            }
        }

        private void OnKeyDownPageHandler(object sender, System.Windows.Input.KeyEventArgs e) {
            if (e.Key == Key.Left) loadPrevImage(null, null);
            if (e.Key == Key.Right) loadNextImage(null, null);
        }

        private void showInfoActiveImage(object sender, RoutedEventArgs e) {
            Info.Main imgInfo = new Info.Main();

            imgInfo.loadImageInfo(activeImage);

            frmImageInfo.Content = imgInfo;
        }

        private void ExitApplication(object sender, RoutedEventArgs e) {
            System.Windows.Application.Current.Shutdown();
        }

        private void EditActiveImage(object sender, RoutedEventArgs e) {
            Editor.Main editorPage = new Editor.Main(activeImagePath);
            var mainWindow = (MainWindow)System.Windows.Application.Current.MainWindow;
            mainWindow.frmMain.Content = editorPage;
        }

        private void OpenProjectInEditor(string filePath) {
            Editor.Main editorPage = new Editor.Main(filePath);
            var mainWindow = (MainWindow)System.Windows.Application.Current.MainWindow;
            mainWindow.frmMain.Content = editorPage;
        }
    }
}
